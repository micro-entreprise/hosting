from unittest import mock

from hosting.prometheus import notify, notify_pushgateway


@mock.patch("hosting.prometheus.notify_pushgateway")
def test_parser(prom_notify_mock):
    with mock.patch(
        "sys.argv",
        [
            "prom-notify-prog",
            "--prometheus-metric",
            "job_time_serie_metric",
            "--prometheus-instance",
            "my-instance",
            "--prometheus-user",
            "username",
            "--prometheus-password",
            "password",
            "https://prom.gateway.domain",
            "job name",
        ],
    ):
        notify()
    prom_notify_mock.assert_called_with(
        gateway_uri="https://prom.gateway.domain",
        job_name="job name",
        metric="job_time_serie_metric",
        instance="my-instance",
        user="username",
        password="password",
    )


@mock.patch("hosting.prometheus.push_to_gateway")
def test_config_without_auth(mock_push):
    with mock.patch("hosting.prometheus.CollectorRegistry") as mocked_registry:
        notify_pushgateway(
            gateway_uri="https://prom.gateway.domain",
            job_name="job name",
            metric="job_time_serie_metric",
            instance="my-instance",
        )
        mock_push.assert_called_with(
            "https://prom.gateway.domain",
            job="job name",
            registry=mocked_registry(),
            handler=None,
            grouping_key={"instance": "my-instance"},
        )


@mock.patch("hosting.prometheus.push_to_gateway")
def test_config_without_auth_nor_instance(mock_push):
    with mock.patch("hosting.prometheus.CollectorRegistry") as mocked_registry:
        notify_pushgateway(
            gateway_uri="https://prom.gateway.domain",
            job_name="job name",
            metric="job_time_serie_metric",
        )
        mock_push.assert_called_with(
            "https://prom.gateway.domain",
            job="job name",
            registry=mocked_registry(),
            handler=None,
            grouping_key={},
        )


@mock.patch("hosting.prometheus.push_to_gateway")
def test_config_with_auth(mock_push):
    notify_pushgateway(
        gateway_uri="https://prom.gateway.domain",
        job_name="job name",
        metric="job_time_serie_metric",
        user="abc",
        password="efg",
    )
    assert (
        mock_push.mock_calls[0].kwargs["handler"] is not None
    ), "auth basic handler should be defined"
