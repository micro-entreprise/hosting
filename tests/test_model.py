import json
from datetime import datetime
from pathlib import Path
from unittest import mock

import pytest
from freezegun import freeze_time
from pydantic.json import pydantic_encoder

from hosting.model import FoundFile, SearchPattern, SearchResult
from hosting.parser_util import rules_from_str


def test_search_pattern():
    yaml_data = """
    rules:
      - name: test 1
        glob_pattern: test glob_pattern
        include_pattern: test include_pattern
        exclude_pattern: test exclude_pattern
        delta_from: 123.5
        delta_to: 456.7
        from_date: "2021-06-01T18:50:00"
        to_date: "2020-06-15T13:03:40"
      - name: test 2
        glob_pattern: "**/*"
        include_pattern: ".*"
        exclude_pattern: ".*"
    """
    rules_from_yaml = rules_from_str(yaml_data)
    json_data = json.dumps(rules_from_yaml, default=pydantic_encoder)
    rules_from_json = rules_from_str(json_data)
    assert rules_from_yaml.rules == rules_from_json.rules


@pytest.mark.parametrize(
    "values,mock_date,expected_from_dt,expected_to_dt",
    [
        ({}, "1984-06-20", None, None),
        (
            {"delta_from": 3 * 24 * 60 * 60, "delta_to": 5 * 24 * 60 * 60},
            "1984-06-20 12:20:30",
            datetime(1984, 6, 17, 12, 20, 30),
            datetime(1984, 6, 15, 12, 20, 30),
        ),
        (
            {
                "from_date": datetime(1994, 6, 20, 12, 20, 30),
                "to_date": datetime(1989, 6, 20, 12, 20, 30),
                "delta_from": 3 * 24 * 60 * 60,
                "delta_to": 5 * 24 * 60 * 60,
            },
            "2014-06-20 12:20:30",
            datetime(1994, 6, 17, 12, 20, 30),
            datetime(1989, 6, 15, 12, 20, 30),
        ),
        (
            {
                "from_date": datetime(1994, 6, 20, 12, 20, 30),
                "to_date": datetime(1989, 6, 20, 12, 20, 30),
            },
            "2014-06-20 12:20:30",
            datetime(1994, 6, 20, 12, 20, 30),
            datetime(1989, 6, 20, 12, 20, 30),
        ),
    ],
)
def test_search_pattern_property_no_params(
    values, mock_date, expected_from_dt, expected_to_dt
):
    sp = SearchPattern(**values)
    with freeze_time(mock_date):
        assert sp.from_dt == expected_from_dt
        assert sp.to_dt == expected_to_dt


def test_empty_obj_str():
    assert str(FoundFile()) == ""
    assert str(SearchResult(Path("/"))) == ""


def test_search_result():
    r1 = SearchPattern(name="r1")
    r2 = SearchPattern(name="r2")

    P1 = mock.MagicMock(
        spec=Path,
        **{
            "stat.return_value.st_mtime": 0,
            "__str__.return_value": "/test/f1",
            "relative_to.return_value": "f1",
        },
    )

    P2 = mock.MagicMock(
        spec=Path,
        **{
            "stat.return_value.st_mtime": 0,
            "__str__.return_value": "/test/f2",
            "relative_to.return_value": "f2",
        },
    )

    P3 = mock.MagicMock(
        spec=Path,
        **{
            "stat.return_value.st_mtime": 0,
            "__str__.return_value": "/test/f3",
            "relative_to.return_value": "f3",
        },
    )

    sr = SearchResult(Path("/test"))
    sr.append(P3, r1)
    sr.append(P1, r1)
    sr.append(P2, r2)
    sr.append(P1, r2)
    sr.append(P2, r1)
    # compute date as it depeds on tz of the host running
    # test
    datestr = datetime.fromtimestamp(0).isoformat()
    assert sr.display(verbose=True) == (
        f"f1         {datestr}       r1, r2\n"
        f"f2         {datestr}       r2, r1\n"
        f"f3         {datestr}       r1"
    )
    assert sr.paths == [P3, P1, P2]
    sr.sort(key=lambda found_file: str(found_file.file))
    assert sr.paths == [P1, P2, P3]
