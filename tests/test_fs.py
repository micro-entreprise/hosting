import tempfile
from datetime import datetime, timedelta
from io import StringIO
from pathlib import Path
from unittest import mock

import pytest
from conftest import set_file_time

from hosting.fs import get_files, get_latest_file, search


@mock.patch("sys.stdout", new_callable=StringIO)
@mock.patch(
    "hosting.fs.get_files",
    return_value=[
        mock.MagicMock(
            spec=Path,
            **{
                "stat.return_value.st_mtime": 1,
                "__str__.return_value": "MyDir/test1",
                "relative_to.return_value": "test1",
            },
        ),
        mock.MagicMock(
            spec=Path,
            **{
                "stat.return_value.st_mtime": 2,
                "__str__.return_value": "MyDir/test2",
                "relative_to.return_value": "test2",
            },
        ),
    ],
)
def test_parser_latest(gf, mo_stdout):
    with mock.patch(
        "sys.argv",
        [
            "purge-prog",
            "-l",
            "DEBUG",
            "-g",
            "pat**/*",
            "-i",
            "re-in",
            "-e",
            "re-ex",
            "-df",
            "2020-06-15 12",
            "-dt",
            "2019-06-01 06:12",
            "--last",
            "1",
            "MyDir",
        ],
    ):
        search()
    assert mo_stdout.getvalue() == "test2\n"
    gf.assert_called_once_with(
        Path("MyDir"),
        glob_pattern="pat**/*",
        re_pattern="re-in",
        exclude_re_pattern="re-ex",
        from_dt=datetime(2020, 6, 15, 12),
        to_dt=datetime(2019, 6, 1, 6, 12),
        sorted_method=None,
        file_only=True,
    )


@mock.patch("sys.stdout", new_callable=StringIO)
@mock.patch(
    "hosting.fs.get_files",
    return_value=[
        mock.MagicMock(
            spec=Path,
            **{
                "stat.return_value.st_mtime": 1,
                "__str__.return_value": "MyDir/test1",
                "relative_to.return_value": "test1",
            },
        ),
        mock.MagicMock(
            spec=Path,
            **{
                "stat.return_value.st_mtime": 2,
                "__str__.return_value": "MyDir/test2",
                "relative_to.return_value": "test2",
            },
        ),
    ],
)
def test_parser(gf, mo_stdout):
    with mock.patch(
        "sys.argv",
        [
            "purge-prog",
            "-l",
            "DEBUG",
            "-g",
            "pat**/*",
            "-i",
            "re-in",
            "-e",
            "re-ex",
            "-df",
            "2020-06-15 12",
            "-dt",
            "2019-06-01 06:12",
            "-wd",
            "MyDir",
        ],
    ):
        search()
    gf.assert_called_once_with(
        Path("MyDir"),
        glob_pattern="pat**/*",
        re_pattern="re-in",
        exclude_re_pattern="re-ex",
        from_dt=datetime(2020, 6, 15, 12),
        to_dt=datetime(2019, 6, 1, 6, 12),
        sorted_method=None,
        file_only=False,
    )
    assert mo_stdout.getvalue() == "test1\ntest2\n"


def test_get_latest_file_not_a_directory():
    path = b"/some/file/that/does/not/exists"
    with pytest.raises(NotADirectoryError) as err:
        get_latest_file(path)
    assert str(err.value) == f"Could you ensure directory exists: {path.decode()}"


def test_get_latest_file_not_a_directory_it_is_a_file():
    with tempfile.NamedTemporaryFile() as temp:
        with pytest.raises(NotADirectoryError) as err:
            get_latest_file(temp.name)
    assert str(err.value) == f"Could you ensure directory exists: {temp.name}"


@pytest.mark.parametrize(
    "files,pattern,expected",
    [
        pytest.param(
            ["test.py"],
            None,
            "test.py",
            id="one-file",
        ),
        pytest.param(
            ["test.py", "test2.py"],
            None,
            "test2.py",
            id="two-files",
        ),
        pytest.param(
            ["test.py", "test2.py"],
            "*t.py",
            "test.py",
            id="two-files-pattern",
        ),
    ],
)
def test_get_latest_file(tmpdir, files, pattern, expected):
    directory = Path(str(tmpdir))
    expected_file = directory.joinpath(expected)
    d = datetime.now()
    for file in files:
        f = directory.joinpath(file)
        f.touch()
        d += timedelta(days=1)
        set_file_time(f, d)
    assert expected_file == get_latest_file(directory, glob_pattern=pattern)


def test_get_latest_file_an_empty_dir(tmpdir):
    assert get_latest_file(Path(str(tmpdir))) is None


def test_get_files_with_directorires(tmpdir):
    directory = Path(str(tmpdir))
    directory.joinpath("dir1").mkdir()
    directory.joinpath("dir2").mkdir()
    directory.joinpath("dir3").mkdir()
    directory.joinpath("dir3", "directory").mkdir()
    for p, mtime in [
        (directory.joinpath("dir3", "file1"), datetime(2020, 6, 20)),
        (directory.joinpath("dir2", "file2"), datetime(2020, 6, 1)),
        (directory.joinpath("file3"), datetime(2020, 6, 15)),
    ]:
        p.touch()
        set_file_time(p, mtime)
    assert get_files(directory, glob_pattern="**/*") == [
        directory.joinpath("dir2", "file2"),
        directory.joinpath("file3"),
        directory.joinpath("dir3", "file1"),
    ]


def test_get_files_with_re_pattern(tmpdir):
    directory = Path(str(tmpdir))
    directory.joinpath("directory").mkdir()
    for p, mtime in [
        (directory.joinpath("file1.dump"), datetime(2020, 6, 1)),
        (directory.joinpath("file1.sql.gz"), datetime(2020, 6, 15)),
        (directory.joinpath("file1.sql.tar"), datetime(2020, 6, 15)),
        (directory.joinpath("file1.py"), datetime(2020, 6, 16)),
        (directory.joinpath("file1.sql"), datetime(2020, 6, 20)),
        (directory.joinpath("sql"), datetime(2020, 6, 20)),
        (directory.joinpath("directory", "file1.sql"), datetime(2020, 6, 21)),
    ]:
        p.touch()
        set_file_time(p, mtime)

    assert get_files(directory, re_pattern=".*\.(dump|sql.gz|sql)$") == [
        directory.joinpath("file1.dump"),
        directory.joinpath("file1.sql.gz"),
        directory.joinpath("file1.sql"),
    ]
