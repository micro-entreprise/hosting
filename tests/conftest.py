import os
from datetime import datetime
from pathlib import Path

import pytest


def set_file_time(path: Path, mtime: datetime):
    """Set the modification time of a given filename to the given mtime.
    Inspired from:
    https://techoverflow.net/2019/07/22/how-to-set-file-modification-time-mtime-in-python/
    """
    os.utime(str(path), times=(mtime.timestamp(), mtime.timestamp()))


@pytest.fixture()
def files():
    return [
        ("dbname-2017-01-01T010502.415612.sql.gz", datetime(2017, 1, 1, 1, 5, 2)),
        ("dbname-2018-01-01T010502.415612.sql.gz", datetime(2018, 1, 1, 1, 5, 2)),
        ("dbname-2018-02-01T010502.415612.sql.gz", datetime(2018, 2, 1, 1, 5, 2)),
        ("dbname-2018-03-01T010502.415612.sql.gz", datetime(2018, 3, 1, 1, 5, 2)),
        ("dbname-2018-04-01T010502.415612.sql.gz", datetime(2018, 4, 1, 1, 5, 2)),
        ("dbname-2018-05-01T010502.415612.sql.gz", datetime(2018, 5, 1, 1, 5, 2)),
        ("dbname-2018-06-01T010502.415612.sql.gz", datetime(2018, 6, 1, 1, 5, 2)),
        ("dbname-2018-07-01T010502.415612.sql.gz", datetime(2018, 7, 1, 1, 5, 2)),
        ("dbname-2018-08-01T010502.415612.sql.gz", datetime(2018, 8, 1, 1, 5, 2)),
        ("dbname-2018-09-01T010502.415612.sql.gz", datetime(2018, 9, 1, 1, 5, 2)),
        ("dbname-2018-10-01T010502.415612.sql.gz", datetime(2018, 10, 1, 1, 5, 2)),
        ("dbname-2018-11-01T010502.415612.sql.gz", datetime(2018, 11, 1, 1, 5, 2)),
        ("dbname-2018-12-01T010502.415612.sql.gz", datetime(2018, 12, 1, 1, 5, 2)),
        ("dbname-2019-01-01T010502.454884.sql.gz", datetime(2019, 1, 1, 1, 5, 2)),
        ("dbname-2019-02-01T010502.454884.sql.gz", datetime(2019, 2, 1, 1, 5, 2)),
        ("dbname-2019-03-01T010502.454884.sql.gz", datetime(2019, 3, 1, 1, 5, 2)),
        ("dbname-2019-04-01T010502.454884.sql.gz", datetime(2019, 4, 1, 1, 5, 2)),
        ("dbname-2019-05-01T010502.454884.sql.gz", datetime(2019, 5, 1, 1, 5, 2)),
        ("dbname-2019-06-01T010502.454884.sql.gz", datetime(2019, 6, 1, 1, 5, 2)),
        ("dbname-2019-07-01T010502.454884.sql.gz", datetime(2019, 7, 1, 1, 5, 2)),
        ("dbname-2019-08-01T010502.454884.sql.gz", datetime(2019, 8, 1, 1, 5, 2)),
        ("dbname-2019-09-01T010502.454884.sql.gz", datetime(2019, 9, 1, 1, 5, 2)),
        ("dbname-2019-10-01T010502.454884.sql.gz", datetime(2019, 10, 1, 1, 5, 2)),
        ("dbname-2019-11-01T010502.454884.sql.gz", datetime(2019, 11, 1, 1, 5, 2)),
        ("dbname-2019-12-01T010502.454884.sql.gz", datetime(2019, 12, 1, 1, 5, 2)),
        ("dbname-2020-01-01T010502.454884.sql.gz", datetime(2020, 1, 1, 1, 5, 2)),
        ("dbname-2020-01-15T001634.922638.sql.gz", datetime(2020, 1, 15, 0, 16, 34)),
        ("dbname-2020-02-01T010502.454884.sql.gz", datetime(2020, 2, 1, 1, 5, 2)),
        ("dbname-2020-02-15T001634.922638.sql.gz", datetime(2020, 2, 15, 0, 16, 34)),
        ("dbname-2020-03-01T010502.454884.sql.gz", datetime(2020, 3, 1, 1, 5, 2)),
        ("dbname-2020-03-15T001634.922638.sql.gz", datetime(2020, 3, 15, 0, 16, 34)),
        ("dbname-2020-04-01T010502.454884.sql.gz", datetime(2020, 4, 1, 1, 5, 2)),
        ("dbname-2020-04-15T011634.922638.sql.gz", datetime(2020, 4, 15, 1, 16, 34)),
        ("dbname-2020-05-01T010502.454884.sql.gz", datetime(2020, 5, 1, 1, 5, 2)),
        ("dbname-2020-05-15T011634.922638.sql.gz", datetime(2020, 5, 15, 1, 16, 34)),
        ("dbname-2020-06-01T010502.454884.sql.gz", datetime(2020, 6, 1, 1, 5, 2)),
        ("dbname-2020-06-15T001634.922638.sql.gz", datetime(2020, 6, 15, 0, 16, 34)),
        ("dbname-2020-07-01T010502.454884.sql.gz", datetime(2020, 7, 1, 1, 5, 2)),
        ("dbname-2020-07-15T021634.922638.sql.gz", datetime(2020, 7, 15, 2, 16, 34)),
        ("dbname-2020-08-01T010502.454884.sql.gz", datetime(2020, 8, 1, 1, 5, 2)),
        ("dbname-2020-08-15T021634.922638.sql.gz", datetime(2020, 8, 15, 2, 16, 34)),
        ("dbname-2020-09-01T010502.454884.sql.gz", datetime(2020, 9, 1, 1, 5, 2)),
        ("dbname-2020-09-15T021634.922638.sql.gz", datetime(2020, 9, 15, 2, 16, 34)),
        ("dbname-2020-10-01T010502.454884.sql.gz", datetime(2020, 10, 1, 1, 5, 2)),
        ("dbname-2020-10-15T021634.922638.sql.gz", datetime(2020, 10, 15, 2, 16, 34)),
        ("dbname-2020-11-01T010502.454884.sql.gz", datetime(2020, 11, 1, 1, 5, 2)),
        ("dbname-2020-11-15T021634.922638.sql.gz", datetime(2020, 11, 15, 2, 16, 34)),
        ("dbname-2020-12-01T010502.454884.sql.gz", datetime(2020, 12, 1, 1, 5, 2)),
        ("dbname-2020-12-15T021634.922638.sql.gz", datetime(2020, 12, 15, 2, 16, 34)),
        ("dbname-2021-01-01T010502.454884.sql.gz", datetime(2021, 1, 1, 1, 5, 2)),
        ("dbname-2021-01-15T021634.922638.sql.gz", datetime(2021, 1, 15, 2, 16, 34)),
        ("dbname-2021-02-01T016542.454884.sql.gz", datetime(2021, 2, 1, 1, 45, 42)),
        ("dbname-2021-02-15T015213.922638.sql.gz", datetime(2021, 2, 15, 1, 52, 13)),
        ("dbname-2021-03-01T005502.454884.sql.gz", datetime(2021, 3, 1, 0, 55, 2)),
        ("dbname-2021-03-11T015502.454884.sql.gz", datetime(2021, 3, 11, 1, 55, 2)),
        ("dbname-2021-03-11T130449.056550.sql.gz", datetime(2021, 3, 11, 13, 4, 49)),
        ("dbname-2021-03-12T014039.774330.sql.gz", datetime(2021, 3, 12, 1, 40, 39)),
        ("dbname-2021-03-12T131317.290000.sql.gz", datetime(2021, 3, 12, 13, 13, 17)),
        ("dbname-2021-03-13T015437.650243.sql.gz", datetime(2021, 3, 13, 1, 54, 37)),
        ("dbname-2021-03-13T135311.081681.sql.gz", datetime(2021, 3, 13, 13, 53, 11)),
        ("dbname-2021-03-14T013751.388960.sql.gz", datetime(2021, 3, 14, 1, 37, 51)),
        ("dbname-2021-03-14T133030.679800.sql.gz", datetime(2021, 3, 14, 13, 30, 30)),
        ("dbname-2021-03-15T001634.922638.sql.gz", datetime(2021, 3, 15, 0, 16, 34)),
        ("dbname-2021-03-15T135344.686087.sql.gz", datetime(2021, 3, 15, 13, 53, 44)),
        ("dbname-2021-03-16T010938.642155.sql.gz", datetime(2021, 3, 16, 1, 9, 38)),
        ("dbname-2021-03-16T133244.307804.sql.gz", datetime(2021, 3, 16, 13, 32, 44)),
        ("dbname-2021-03-17T010939.443546.sql.gz", datetime(2021, 3, 17, 1, 9, 39)),
        ("dbname-2021-03-17T131410.541994.sql.gz", datetime(2021, 3, 17, 13, 14, 10)),
        ("dbname-2021-03-18T015744.197223.sql.gz", datetime(2021, 3, 18, 1, 57, 44)),
        ("dbname-2021-03-18T135739.406218.sql.gz", datetime(2021, 3, 18, 13, 57, 39)),
        ("dbname-2021-03-19T010737.851970.sql.gz", datetime(2021, 3, 19, 1, 7, 37)),
        ("dbname-2021-03-19T131702.354607.sql.gz", datetime(2021, 3, 19, 13, 17, 2)),
        ("dbname-2021-03-20T011144.214407.sql.gz", datetime(2021, 3, 20, 1, 11, 44)),
        ("dbname-2021-03-20T133421.105673.sql.gz", datetime(2021, 3, 20, 13, 34, 21)),
        ("dbname-2021-03-21T014651.494380.sql.gz", datetime(2021, 3, 21, 1, 46, 51)),
        ("dbname-2021-03-21T134502.203365.sql.gz", datetime(2021, 3, 21, 13, 45, 2)),
        ("dbname-2021-03-22T010544.288309.sql.gz", datetime(2021, 3, 22, 1, 5, 44)),
        ("dbname-2021-03-22T132800.419372.sql.gz", datetime(2021, 3, 22, 13, 28, 0)),
        ("dbname-2021-03-23T011144.214407.sql.gz", datetime(2021, 3, 23, 1, 11, 44)),
        ("dbname-2021-03-23T163421.105673.sql.gz", datetime(2021, 3, 23, 16, 34, 21)),
        ("dbname-2021-03-24T014345.975958.sql.gz", datetime(2021, 3, 24, 1, 43, 45)),
        ("dbname-2021-03-24T134645.545742.sql.gz", datetime(2021, 3, 24, 13, 46, 45)),
        ("dbname-2021-03-25T013222.254953.sql.gz", datetime(2021, 3, 25, 1, 32, 22)),
        ("dbname-2021-03-25T132743.179061.sql.gz", datetime(2021, 3, 25, 13, 27, 43)),
        ("dbname-2021-03-26T012038.459229.sql.gz", datetime(2021, 3, 26, 1, 20, 38)),
        ("dbname-2021-03-26T132546.995755.sql.gz", datetime(2021, 3, 26, 13, 25, 46)),
        ("dbname-2021-03-27T014345.975958.sql.gz", datetime(2021, 3, 27, 1, 43, 45)),
        ("dbname-2021-03-27T134645.545742.sql.gz", datetime(2021, 3, 27, 13, 46, 45)),
        ("dbname-2021-03-28T013222.254953.sql.gz", datetime(2021, 3, 28, 1, 32, 22)),
        ("dbname-2021-03-28T132743.179061.sql.gz", datetime(2021, 3, 28, 13, 27, 43)),
        ("dbname-2021-03-29T012038.459229.sql.gz", datetime(2021, 3, 29, 1, 20, 38)),
        ("dbname-2021-03-29T132546.995755.sql.gz", datetime(2021, 3, 29, 13, 25, 46)),
        ("dbname-2021-03-30T014744.687122.sql.gz", datetime(2021, 3, 30, 1, 47, 44)),
        ("dbname-2021-03-30T133744.283206.sql.gz", datetime(2021, 3, 30, 13, 37, 44)),
        ("dbname-2021-03-31T010501.987911.sql.gz", datetime(2021, 3, 31, 1, 5, 1)),
        ("dbname-2021-03-31T133810.636163.sql.gz", datetime(2021, 3, 31, 13, 38, 10)),
        ("dbname-2021-04-01T025502.454884.sql.gz", datetime(2021, 4, 1, 2, 55, 2)),
        ("dbname-2021-04-01T130449.056550.sql.gz", datetime(2021, 4, 1, 13, 4, 49)),
        ("dbname-2021-04-02T014039.774330.sql.gz", datetime(2021, 4, 2, 1, 40, 39)),
        ("dbname-2021-04-02T131317.290000.sql.gz", datetime(2021, 4, 2, 13, 13, 17)),
        ("dbname-2021-04-03T015437.650243.sql.gz", datetime(2021, 4, 3, 1, 54, 37)),
        ("dbname-2021-04-03T135311.081681.sql.gz", datetime(2021, 4, 3, 13, 53, 11)),
        ("dbname-2021-04-04T013751.388960.sql.gz", datetime(2021, 4, 4, 1, 37, 51)),
        ("dbname-2021-04-04T133030.679800.sql.gz", datetime(2021, 4, 4, 13, 30, 30)),
        ("dbname-2021-04-05T011634.922638.sql.gz", datetime(2021, 4, 5, 1, 16, 34)),
        ("dbname-2021-04-05T135344.686087.sql.gz", datetime(2021, 4, 5, 13, 53, 44)),
        ("dbname-2021-04-06T010938.642155.sql.gz", datetime(2021, 4, 6, 1, 9, 38)),
        ("dbname-2021-04-06T133244.307804.sql.gz", datetime(2021, 4, 6, 13, 32, 44)),
        ("dbname-2021-04-07T010939.443546.sql.gz", datetime(2021, 4, 7, 1, 9, 39)),
        ("dbname-2021-04-07T131410.541994.sql.gz", datetime(2021, 4, 7, 13, 14, 10)),
        ("dbname-2021-04-08T015744.197223.sql.gz", datetime(2021, 4, 8, 1, 57, 44)),
        ("dbname-2021-04-08T135739.406218.sql.gz", datetime(2021, 4, 8, 13, 57, 39)),
        ("dbname-2021-04-09T010737.851970.sql.gz", datetime(2021, 4, 9, 1, 7, 37)),
        ("dbname-2021-04-09T131702.354607.sql.gz", datetime(2021, 4, 9, 13, 17, 2)),
        ("dbname-2021-04-10T011144.214407.sql.gz", datetime(2021, 4, 10, 1, 11, 44)),
        ("dbname-2021-04-10T133421.105673.sql.gz", datetime(2021, 4, 10, 13, 34, 21)),
        ("dbname-2021-04-11T014651.494380.sql.gz", datetime(2021, 4, 11, 1, 46, 51)),
        ("dbname-2021-04-11T134502.203365.sql.gz", datetime(2021, 4, 11, 13, 45, 2)),
        ("dbname-2021-04-12T010544.288309.sql.gz", datetime(2021, 4, 12, 1, 5, 44)),
        ("dbname-2021-04-12T132800.419372.sql.gz", datetime(2021, 4, 12, 13, 28, 0)),
        ("dbname-2021-04-13T011144.214407.sql.gz", datetime(2021, 4, 13, 1, 11, 44)),
        ("dbname-2021-04-13T133421.105673.sql.gz", datetime(2021, 4, 13, 13, 34, 21)),
        ("dbname-2021-04-14T014651.494380.sql.gz", datetime(2021, 4, 14, 1, 46, 51)),
        ("dbname-2021-04-14T134502.203365.sql.gz", datetime(2021, 4, 14, 13, 45, 2)),
        ("dbname-2021-04-15T010544.288309.sql.gz", datetime(2021, 4, 15, 1, 5, 44)),
        ("dbname-2021-04-15T132800.419372.sql.gz", datetime(2021, 4, 15, 13, 28, 0)),
    ]


@pytest.fixture()
def fullfill_tmpdir(tmpdir, files):
    p = Path(str(tmpdir))
    for f, dt in files:
        path = p.joinpath(f)
        path.touch()
        set_file_time(path, dt)
    return tmpdir
