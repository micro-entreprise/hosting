from pathlib import Path
from unittest import mock

import pytest
from freezegun import freeze_time

from hosting.cleaner import Purger, main
from hosting.fs import get_files
from hosting.model import SearchPattern, SearchPatternRules
from hosting.parser_util import default_rules


@pytest.fixture()
def expected_result_2021_4_15():
    return [
        "dbname-2017-01-01T010502.415612.sql.gz",
        "dbname-2018-01-01T010502.415612.sql.gz",
        "dbname-2019-01-01T010502.454884.sql.gz",
        "dbname-2020-01-01T010502.454884.sql.gz",
        "dbname-2020-05-01T010502.454884.sql.gz",
        "dbname-2020-06-01T010502.454884.sql.gz",
        "dbname-2020-07-01T010502.454884.sql.gz",
        "dbname-2020-08-01T010502.454884.sql.gz",
        "dbname-2020-09-01T010502.454884.sql.gz",
        "dbname-2020-10-01T010502.454884.sql.gz",
        "dbname-2020-11-01T010502.454884.sql.gz",
        "dbname-2020-12-01T010502.454884.sql.gz",
        "dbname-2021-01-01T010502.454884.sql.gz",
        "dbname-2021-02-01T016542.454884.sql.gz",
        "dbname-2021-02-15T015213.922638.sql.gz",
        "dbname-2021-03-01T005502.454884.sql.gz",
        "dbname-2021-03-15T001634.922638.sql.gz",
        "dbname-2021-03-16T010938.642155.sql.gz",
        "dbname-2021-03-17T010939.443546.sql.gz",
        "dbname-2021-03-18T015744.197223.sql.gz",
        "dbname-2021-03-19T010737.851970.sql.gz",
        "dbname-2021-03-20T011144.214407.sql.gz",
        "dbname-2021-03-21T014651.494380.sql.gz",
        "dbname-2021-03-22T010544.288309.sql.gz",
        "dbname-2021-03-23T011144.214407.sql.gz",
        "dbname-2021-03-24T014345.975958.sql.gz",
        "dbname-2021-03-25T013222.254953.sql.gz",
        "dbname-2021-03-26T012038.459229.sql.gz",
        "dbname-2021-03-27T014345.975958.sql.gz",
        "dbname-2021-03-28T013222.254953.sql.gz",
        "dbname-2021-03-29T012038.459229.sql.gz",
        "dbname-2021-03-30T014744.687122.sql.gz",
        "dbname-2021-03-31T010501.987911.sql.gz",
        "dbname-2021-04-01T025502.454884.sql.gz",
        "dbname-2021-04-02T014039.774330.sql.gz",
        "dbname-2021-04-03T015437.650243.sql.gz",
        "dbname-2021-04-04T013751.388960.sql.gz",
        "dbname-2021-04-05T011634.922638.sql.gz",
        "dbname-2021-04-06T010938.642155.sql.gz",
        "dbname-2021-04-07T010939.443546.sql.gz",
        "dbname-2021-04-08T015744.197223.sql.gz",
        "dbname-2021-04-09T010737.851970.sql.gz",
        "dbname-2021-04-09T131702.354607.sql.gz",
        "dbname-2021-04-10T011144.214407.sql.gz",
        "dbname-2021-04-10T133421.105673.sql.gz",
        "dbname-2021-04-11T014651.494380.sql.gz",
        "dbname-2021-04-11T134502.203365.sql.gz",
        "dbname-2021-04-12T010544.288309.sql.gz",
        "dbname-2021-04-12T132800.419372.sql.gz",
        "dbname-2021-04-13T011144.214407.sql.gz",
        "dbname-2021-04-13T133421.105673.sql.gz",
        "dbname-2021-04-14T014651.494380.sql.gz",
        "dbname-2021-04-14T134502.203365.sql.gz",
        "dbname-2021-04-15T010544.288309.sql.gz",
        "dbname-2021-04-15T132800.419372.sql.gz",
    ]


@freeze_time("2021-04-15 14:31:33")
def test_purge(fullfill_tmpdir, expected_result_2021_4_15):
    directory = Path(str(fullfill_tmpdir))
    purger = Purger(root_directory=directory, patterns=default_rules())
    purger.clean()
    assert [f.name for f in get_files(directory)] == expected_result_2021_4_15


@freeze_time("2021-04-15 14:31:33")
def test_dry_run_purge(fullfill_tmpdir, files):
    directory = Path(str(fullfill_tmpdir))
    purger = Purger(root_directory=directory, patterns=default_rules(), dry_run=True)
    purger.clean()
    assert [f.name for f in get_files(directory)] == [name for name, _ in files]


@mock.patch("hosting.cleaner.Purger.clean")
def test_parser(purger_clean):
    with mock.patch(
        "sys.argv",
        [
            "purge-prog",
            "-l",
            "DEBUG",
            "-n",
            "-g",
            "*",
            "/somedir",
        ],
    ):
        purger = main(testing=True)
    purger_clean.assert_called_once()
    assert purger.root_directory == Path("/somedir")
    assert purger.dry_run is True
    assert purger.patterns.rules[0].glob_pattern == "*"


@freeze_time("2021-04-15 14:31:33")
def test_purge_directory(fullfill_tmpdir, expected_result_2021_4_15):
    directory = Path(str(fullfill_tmpdir))
    patterns = SearchPatternRules(
        rules=[
            SearchPattern(
                name="main directory",
                glob_pattern="./**/",
                exclude_pattern=None,
                delta_from=None,
                delta_to=None,
                file_only=False,
            ),
        ],
    )
    purger = Purger(root_directory=directory, patterns=patterns)
    purger.clean()
    assert not directory.exists()
