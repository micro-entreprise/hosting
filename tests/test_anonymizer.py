import subprocess
from pathlib import Path
from unittest import mock

import pytest

from hosting.anonymizer import Anonymize, anonymize


@mock.patch("hosting.anonymizer.Anonymize.run")
def test_parser(anon_run):
    with mock.patch(
        "sys.argv",
        [
            "ano-prog",
            "-l",
            "DEBUG",
            "-n",
            "-w",
            "echo '{command}'",
            "-H",
            "host",
            "-p",
            "port",
            "-d",
            "dbname",
            "-U",
            "user",
            "--extra-pg-restore",
            "restore_extra",
            "--extra-pg-dump",
            "dump_extra",
            "--extra-psql",
            "psql extra",
            "--force-overwrite",
            "in",
            "sql",
            "out",
        ],
    ):
        anon = anonymize(testing=True)
    anon_run.assert_called_once()
    assert anon.input == Path("in")
    assert anon.sql == Path("sql")
    assert anon.output == Path("out")
    assert anon.pg_host == "host"
    assert anon.pg_port == "port"
    assert anon.pg_dbname == "dbname"
    assert anon.pg_user == "user"
    assert anon.pg_restore_extra == "restore_extra"
    assert anon.pg_dump_extra == "dump_extra"
    assert anon.psql_extra == "psql extra"
    assert anon.wrap_cmd == "echo '{command}'"
    assert anon.dry_run is True
    assert anon.force is True


@mock.patch("hosting.anonymizer.Path.exists", side_effect=[True, True, True])
@mock.patch("hosting.anonymizer.Anonymize.dump")
@mock.patch("hosting.anonymizer.Anonymize.anonymize")
@mock.patch("hosting.anonymizer.Anonymize.restore")
def test_workflow(r, a, d, _):
    Anonymize(
        input=Path("in.dump"),
        sql=Path("anonymize.sql"),
        output=Path("out.dump"),
        force=True,
    ).run()
    r.assert_called_once()
    a.assert_called_once()
    d.assert_called_once()


@mock.patch("hosting.anonymizer.Path.exists", side_effect=[True, True, False])
def test_dry_run(_):
    with mock.patch("subprocess.run") as m:
        Anonymize(
            input=Path("in.dump"),
            sql=Path("anonymize.sql"),
            output=Path("out.dump"),
            dry_run=True,
        ).run()
        m.assert_not_called()


@pytest.mark.parametrize(
    "init,cmd,expected",
    [
        pytest.param(
            dict(
                wrap_cmd=None,
            ),
            ["test"],
            [
                mock.call("test", shell=True, check=True, capture_output=True),
            ],
            id="no-template",
        ),
        pytest.param(
            dict(wrap_cmd="docker exec db '{command}'"),
            ["psql -c 'SELECT 1'"],
            [
                mock.call(
                    # have to figure out if quoting properly
                    # works in real life
                    "docker exec db 'psql -c 'SELECT 1''",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
            ],
            id="with-template",
        ),
    ],
)
@mock.patch("subprocess.run")
def test_run_cmd(m, init, cmd, expected):
    Anonymize(**init).run_command(cmd)
    assert m.mock_calls == expected


@pytest.mark.parametrize(
    "params,expected",
    [
        pytest.param(
            dict(
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
            ),
            [
                mock.call(
                    "pg_restore -Fc -O in.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "psql -f anonymize.sql", shell=True, check=True, capture_output=True
                ),
                mock.call(
                    "pg_dump -Fc -O -f out.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
            ],
            id="base",
        ),
        pytest.param(
            dict(
                pg_host="db",
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
            ),
            [
                mock.call(
                    "pg_restore -Fc -O -h db in.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "psql -h db -f anonymize.sql",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "pg_dump -Fc -O -h db -f out.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
            ],
            id="pg_host",
        ),
        pytest.param(
            dict(
                pg_user="me",
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
            ),
            [
                mock.call(
                    "pg_restore -Fc -O -U me in.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "psql -U me -f anonymize.sql",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "pg_dump -Fc -O -U me -f out.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
            ],
            id="pg_user",
        ),
        pytest.param(
            dict(
                pg_port="5432",
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
            ),
            [
                mock.call(
                    "pg_restore -Fc -O -p 5432 in.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "psql -p 5432 -f anonymize.sql",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "pg_dump -Fc -O -p 5432 -f out.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
            ],
            id="pg_port",
        ),
        pytest.param(
            dict(
                pg_dbname="abc",
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
            ),
            [
                mock.call(
                    "pg_restore -Fc -O -d abc in.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "psql -d abc -f anonymize.sql",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "pg_dump -Fc -O -d abc -f out.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
            ],
            id="pg_dbname",
        ),
        pytest.param(
            dict(
                pg_restore_extra="--extra-param",
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
            ),
            [
                mock.call(
                    "pg_restore -Fc -O --extra-param in.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "psql -f anonymize.sql", shell=True, check=True, capture_output=True
                ),
                mock.call(
                    "pg_dump -Fc -O -f out.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
            ],
            id="pg_restore_extra",
        ),
        pytest.param(
            dict(
                pg_dump_extra="--extra-param",
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
            ),
            [
                mock.call(
                    "pg_restore -Fc -O in.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "psql -f anonymize.sql", shell=True, check=True, capture_output=True
                ),
                mock.call(
                    "pg_dump -Fc -O --extra-param -f out.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
            ],
            id="pg_dump_extra",
        ),
        pytest.param(
            dict(
                psql_extra="--extra-param",
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
            ),
            [
                mock.call(
                    "pg_restore -Fc -O in.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "psql --extra-param -f anonymize.sql",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "pg_dump -Fc -O -f out.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
            ],
            id="psql_extra",
        ),
        pytest.param(
            dict(
                pg_host="db",
                pg_port="5432",
                pg_user="me",
                pg_dbname="abc",
                pg_restore_extra="--extra-param-restore",
                pg_dump_extra="--extra-param-dump",
                psql_extra="--extra-param",
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
            ),
            [
                mock.call(
                    "pg_restore -Fc -O -h db -p 5432 -U me -d abc --extra-param-restore in.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "psql -h db -p 5432 -U me -d abc --extra-param -f anonymize.sql",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
                mock.call(
                    "pg_dump -Fc -O -h db -p 5432 -U me -d abc --extra-param-dump -f out.dump",
                    shell=True,
                    check=True,
                    capture_output=True,
                ),
            ],
            id="complet",
        ),
    ],
)
@mock.patch("hosting.anonymizer.Path.exists", side_effect=[True, True, False])
@mock.patch("subprocess.run")
def test_runs(m, _, params, expected):
    Anonymize(**params).run()
    m.assert_has_calls(expected, any_order=False)


@pytest.mark.parametrize(
    "params,exists,except_class,msg",
    [
        pytest.param(
            dict(
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
            ),
            [],
            FileNotFoundError,
            "Input file not found (None)",
            id="no-input",
        ),
        pytest.param(
            dict(
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
            ),
            [False],
            FileNotFoundError,
            "Input file not found (in.dump)",
            id="input-missing-file",
        ),
        pytest.param(
            dict(
                input=Path("in.dump"),
                output=Path("out.dump"),
            ),
            [
                True,
            ],
            FileNotFoundError,
            "Anonymize SQL file not found (None)",
            id="no-sql",
        ),
        pytest.param(
            dict(
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
            ),
            [True, False],
            FileNotFoundError,
            "Anonymize SQL file not found (anonymize.sql)",
            id="sql-missing-file",
        ),
        pytest.param(
            dict(
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
            ),
            [True, True, True],
            FileExistsError,
            "Output file already present (out.dump)",
            id="output-exists",
        ),
        pytest.param(
            dict(
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
            ),
            [True, True],
            ValueError,
            "Output must be a pathlib.Path object (None)",
            id="missing-output",
        ),
    ],
)
@mock.patch("subprocess.run")
def test_exceptions(_, params, exists, except_class, msg):
    with mock.patch("hosting.anonymizer.Path.exists", side_effect=exists):
        with pytest.raises(except_class) as ex:
            Anonymize(**params).run()
        assert str(ex.value) == msg


@mock.patch("hosting.anonymizer.Path.exists", side_effect=[False, False, True])
@mock.patch("hosting.anonymizer.Anonymize.dump")
@mock.patch("hosting.anonymizer.Anonymize.anonymize")
@mock.patch("hosting.anonymizer.Anonymize.restore")
def test_workflow_in_other_context(r, a, d, _):
    Anonymize(
        wrap_cmd="test {command}",
        input=Path("in.dump"),
        sql=Path("anonymize.sql"),
        output=Path("out.dump"),
        force=False,
    ).run()
    r.assert_called_once()
    a.assert_called_once()
    d.assert_called_once()


@pytest.mark.parametrize(
    "side_effect,expected_message",
    [
        (
            subprocess.CalledProcessError(cmd="test", returncode=33),
            "Command 'test' returned non-zero exit status 33.",
        ),
        (
            subprocess.TimeoutExpired(cmd="test timeout", timeout=2),
            "Command 'test timeout' timed out after 2 seconds",
        ),
    ],
)
def test_subprocess_raises(side_effect, expected_message):
    """"""
    with pytest.raises(subprocess.SubprocessError) as err:
        with mock.patch("subprocess.run", side_effect=side_effect):
            Anonymize(
                wrap_cmd="test {command}",
                input=Path("in.dump"),
                sql=Path("anonymize.sql"),
                output=Path("out.dump"),
                force=False,
            ).run()
    assert str(err.value) == expected_message


def test_with_directory_empty(tmpdir):
    with pytest.raises(FileNotFoundError):
        Anonymize(
            input=Path(str(tmpdir)),
            sql=Path("anonymize.sql"),
            output=Path("out.dump"),
            force=False,
        ).run()


@mock.patch("hosting.anonymizer.Path.exists", side_effect=[True, True, False])
@mock.patch("subprocess.run")
def test_output_is_a_dir(m, _, tmpdir):
    tmp = Path(str(tmpdir))
    Anonymize(
        pg_host="db",
        pg_port="5432",
        pg_user="me",
        pg_dbname="abc",
        pg_restore_extra="--extra-param-restore",
        pg_dump_extra="--extra-param-dump",
        psql_extra="--extra-param",
        input=Path("in.dump"),
        sql=Path("anonymize.sql"),
        output=tmp,
    ).run()
    m.assert_has_calls(
        [
            mock.call(
                "pg_restore -Fc -O -h db -p 5432 -U me -d abc --extra-param-restore in.dump",
                shell=True,
                check=True,
                capture_output=True,
            ),
            mock.call(
                "psql -h db -p 5432 -U me -d abc --extra-param -f anonymize.sql",
                shell=True,
                check=True,
                capture_output=True,
            ),
            mock.call(
                "pg_dump -Fc -O -h db -p 5432 -U me -d abc --extra-param-dump "
                f"-f {str(tmp.joinpath('in.dump'))}",
                shell=True,
                check=True,
                capture_output=True,
            ),
        ],
        any_order=False,
    )
