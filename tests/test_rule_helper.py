import json
from datetime import datetime, timedelta
from io import StringIO
from unittest import mock

import pytest
from pydantic.json import pydantic_encoder
from yaml import safe_dump

from hosting.model import SearchPattern, SearchPatternRules
from hosting.parser_util import config_helper, default_rules

test_data = [
    (fmt, params, expected)
    for fmt in ["json", "yaml"]
    for params, expected in [
        ([], default_rules()),
        (
            ["-g", "*/**"],
            SearchPatternRules(
                rules=[SearchPattern(name="Rule from command line", glob_pattern="*/**")]
            ),
        ),
        (
            [
                "-g",
                "*/**",
            ],
            SearchPatternRules(
                rules=[SearchPattern(name="Rule from command line", glob_pattern="*/**")]
            ),
        ),
        (
            [
                "-g",
                "*/**",
                "-i",
                "re-in",
                "-e",
                "re-ex",
                "-df",
                "2020-06-15 14",
                "-ddf",
                "5.5",
                "-dt",
                "2019-06-01 06:12",
                "-ddt",
                "2",
                "-wd",
            ],
            SearchPatternRules(
                rules=[
                    SearchPattern(
                        name="Rule from command line",
                        glob_pattern="*/**",
                        include_pattern="re-in",
                        exclude_pattern="re-ex",
                        from_date=datetime(2020, 6, 15, 14),
                        to_date=datetime(2019, 6, 1, 6, 12),
                        delta_from=timedelta(days=5.5),
                        delta_to=timedelta(days=2),
                        file_only=False,
                    )
                ]
            ),
        ),
    ]
]


@pytest.mark.parametrize("fmt,params,expected", test_data)
@mock.patch("sys.stdout", new_callable=StringIO)
def test_helper_parser(mo_stdout, fmt, params, expected):
    params.insert(0, "prog-test")
    params.append(fmt)
    with mock.patch(
        "sys.argv",
        params,
    ):
        config_helper()

    expected_result = json.dumps(expected, indent=2, default=pydantic_encoder)

    if fmt == "yaml":
        expected_result = safe_dump(json.loads(expected_result), indent=2)
    assert mo_stdout.getvalue() == expected_result + "\n"
