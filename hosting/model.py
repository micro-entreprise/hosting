from dataclasses import field
from datetime import datetime, timedelta
from pathlib import Path
from typing import Callable, Dict, List, Optional

from pydantic.dataclasses import dataclass


@dataclass
class SearchPattern:
    name: Optional[str] = None
    glob_pattern: Optional[str] = None
    include_pattern: Optional[str] = None
    exclude_pattern: Optional[str] = None
    delta_from: Optional[timedelta] = None
    delta_to: Optional[timedelta] = None
    from_date: Optional[datetime] = None
    to_date: Optional[datetime] = None
    file_only: Optional[bool] = True

    @property
    def from_dt(self) -> Optional[datetime]:
        if not self.from_date and not self.delta_from:
            return None
        from_date = self.from_date
        if not from_date:
            from_date = datetime.now()

        return from_date - self.delta_from if self.delta_from else from_date

    @property
    def to_dt(self) -> Optional[datetime]:
        if not self.to_date and not self.delta_to:
            return None
        to_date = self.to_date
        if not to_date:
            to_date = datetime.now()

        return to_date - self.delta_to if self.delta_to else to_date


@dataclass
class SearchPatternRules:
    rules: List[SearchPattern] = field(default_factory=list)


@dataclass
class FoundFile(SearchPatternRules):
    file: Path = None

    def __str__(self):
        return self.display()

    @property
    def rules_name(self):
        return ", ".join([r.name for r in self.rules])

    def display(
        self, verbose: bool = False, root_dir: Path = None, file_format_with: int = 100
    ) -> str:
        if not self.file:
            return ""
        if verbose:
            text = (
                "{file_str:<{file_format_with}} " "{change_date:^30} " "{rules:<}"
            ).format(
                file_str=str(self.file.relative_to(root_dir) if root_dir else self.file),
                file_format_with=file_format_with,
                change_date=datetime.fromtimestamp(
                    self.file.stat().st_mtime
                ).isoformat(),
                rules=self.rules_name,
            )
        else:
            text = str(self.file.relative_to(root_dir) if root_dir else self.file)
        return text


class SearchResult:
    files: List[FoundFile] = None

    root_directory: Path = None

    _index: Dict = None
    """internal index used to aggregate results"""

    @property
    def paths(self):
        return [ff.file for ff in self.files]

    def __init__(self, root_directory):
        self.root_directory = root_directory
        self._index = {}
        self.files = []

    def append(self, file: Path, rule: SearchPattern) -> None:
        if file in self._index:
            if rule not in self._index[file].rules:
                self._index[file].rules.append(rule)
        else:
            self._index[file] = FoundFile(file=file, rules=[rule])
            self.files.append(self._index[file])

    def sort(self, key: Callable = lambda found_file: found_file.file.stat().st_mtime):
        self.files = sorted(self.files, key=key)

    def latest(self, nb=1) -> List[FoundFile]:
        """nb latest elements"""
        self.sort()
        return self.files[-1 * nb :]  # noqa: E203

    def __str__(self):
        return self.display()

    def display(
        self,
        last: int = None,
        verbose=False,
        sorted_key=lambda found_file: str(found_file.file),
    ) -> str:
        """
        :param verbose: display extra informations (date / matching rules)
        :param latest: int number of elements to display
        """
        data = self.files
        if last:
            data = self.latest(nb=last)

        if sorted_key:
            data = sorted(data, key=sorted_key)

        file_format_with = 100
        if verbose:
            longuest = max(
                data,
                key=lambda ff: len(
                    str(
                        ff.file.relative_to(self.root_directory)
                        if self.root_directory
                        else ff.file
                    )
                ),
            )
            if longuest and longuest.file:
                file_format_with = (
                    len(
                        str(
                            longuest.file.relative_to(self.root_directory)
                            if self.root_directory
                            else longuest.file
                        )
                    )
                    + 3
                )

        return "\n".join(
            [
                f.display(
                    verbose=verbose,
                    root_dir=self.root_directory,
                    file_format_with=file_format_with,
                )
                for f in data
            ]
        )
