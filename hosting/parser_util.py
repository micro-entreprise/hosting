import argparse
import json
import os
import sys
import threading
from datetime import datetime, timedelta

import boto3
from boto3.s3.transfer import TransferConfig
from botocore.client import Config
from pydantic.json import pydantic_encoder
from s3path import PureS3Path, register_configuration_parameter
from uri_pathlib_factory import load_pathlib_monkey_patch
from yaml import safe_dump, safe_load

from hosting.model import SearchPattern, SearchPatternRules

load_pathlib_monkey_patch()


def config_helper():
    parser = argparse.ArgumentParser(
        description="Helper to validate/convert SearchPatternRules "
        "or helping generate new entry",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "parser",
        type=str,
        choices=["json", "yaml"],
        help="choose output format",
    )

    rule_params(parser)

    arguments = parser.parse_args()

    json_data = json.dumps(
        parse_rule_args(arguments), indent=2, default=pydantic_encoder
    )
    if arguments.parser == "yaml":
        print(safe_dump(safe_load(json_data), indent=2))
    else:
        print(json_data)


def logging_params(parser):
    logging_group = parser.add_argument_group("Logging params")
    logging_group.add_argument(
        "-f",
        "--logging-file",
        type=argparse.FileType("r"),
        help="Logging configuration file, (logging-level and logging-format "
        "are ignored if provide)",
    )
    logging_group.add_argument("-l", "--logging-level", default="INFO")
    logging_group.add_argument(
        "--logging-format",
        default="%(asctime)s - %(levelname)s (%(module)s%(funcName)s): " "%(message)s",
    )


def pg_params(parser):
    pg_group = parser.add_argument_group("PostgreSql params")
    default_pgsql_host = os.environ.get("PGHOST", None)
    pg_group.add_argument(
        "-H",
        "--host",
        type=str,
        help="postgresql host (default from `PGHOST`)",
        dest="pg_host",
        default=default_pgsql_host,
    )
    default_pgsql_port = os.environ.get("PGPORT", None)
    pg_group.add_argument(
        "-p",
        "--port",
        type=str,
        help="postgresql port (default from `PGPORT`)",
        default=default_pgsql_port,
        dest="pg_port",
    )
    default_pgsql_db = os.environ.get("PGDATABASE", "anon")
    pg_group.add_argument(
        "-d",
        "--dbname",
        type=str,
        help="dbname to use while anonymized the database "
        "(default from `PGDATABASE`)",
        default=default_pgsql_db,
        dest="pg_dbname",
    )
    default_pgsql_user = os.environ.get("PGUSER", None)
    pg_group.add_argument(
        "-U",
        "--username",
        type=str,
        help="Username used to connect to postgresl "
        "(default from `PGUSER`)."
        "Note that password can be provide by PGPASSWORD env or .pgpass cf:"
        "https://stackoverflow.com/questions/2893954/how-to-pass-in-password-to-pg-dump",
        default=default_pgsql_user,
        dest="pg_user",
    )
    pg_group.add_argument(
        "--extra-pg-restore",
        type=str,
        help="List of extra params to use with pg_restore.",
        default="-Fc -O",
        dest="pg_restore_extra",
    )
    pg_group.add_argument(
        "--extra-pg-dump",
        type=str,
        help="List of extra params to use with pg_dump.",
        default="-Fc -O",
        dest="pg_dump_extra",
    )
    pg_group.add_argument(
        "--extra-psql",
        type=str,
        help="List of extra params to use with psql while runing anonymize SQL script.",
        default="",
        dest="psql_extra",
    )


def s3_params(parser):
    group = parser.add_argument_group("S3 configuration")

    group.add_argument(
        "--s3-endpoint-url",
        dest="s3_endpoint_url",
        default="https://s3.fr-par.scw.cloud",
        type=str,
        help="S3 endpoint url",
    )
    group.add_argument(
        "--s3-region-name",
        dest="s3_region_name",
        default="fr-par",
        type=str,
        help="S3 region name.",
    )
    group.add_argument(
        "--s3-access-key", dest="s3_access_key_id", type=str, help="S3 access key ID."
    )
    group.add_argument(
        "--s3-secret-key", dest="s3_secret_access_key", type=str, help="S3 secret key."
    )

    group_s3_transfer = parser.add_argument_group("S3 Transfert configuration")

    group_s3_transfer.add_argument(
        "--s3-multipart-threshold",
        dest="s3_multipart_threshold_mb",
        default=120,
        type=int,
        help=(
            "The transfer size threshold for which "
            "multipart uploads, downloads, and copies will automatically be "
            "triggered.. (MB)"
        ),
    )
    group_s3_transfer.add_argument(
        "--s3-multipart-chunksize",
        dest="s3_multipart_chunksize_mb",
        default=100,
        type=int,
        help=("The partition size of each part for a " "multipart transfer. (MB)"),
    )
    group_s3_transfer.add_argument(
        "--s3-no-threads",
        dest="s3_no_threads",
        action="store_true",
        help=(
            "If False, threads will be used when performing "
            "S3 transfers. If True, no threads will be used in "
            "performing transfers; all logic will be run in the main thread."
        ),
    )
    group_s3_transfer.add_argument(
        "--s3-max-bandwidth",
        dest="s3_max_bandwidth_mb",
        type=int,
        help=(
            "The maximum bandwidth that will be consumed "
            "in uploading and downloading file content. The value is an integer "
            "in terms of Mega bytes per second. (MB)"
        ),
    )

    group_s3_transfer.add_argument(
        "--s3-max-concurrency",
        dest="s3_max_concurrency",
        default=10,
        type=int,
        help=(
            "The maximum number of threads that will be "
            "making requests to perform a transfer. If ``use_threads`` is "
            "set to ``False``, the value provided is ignored as the transfer "
            "will only ever use the main thread. (10)"
        ),
    )
    group_s3_transfer.add_argument(
        "--s3-num-download-attempts",
        dest="s3_num_download_attempts",
        default=5,
        type=int,
        help=(
            "The number of download attempts that "
            "will be retried upon errors with downloading an object in S3. "
            "Note that these retries account for errors that occur when "
            "streaming  down the data from s3 (i.e. socket errors and read "
            "timeouts that occur after receiving an OK response from s3). "
            "Other retryable exceptions such as throttling errors and 5xx "
            "errors are already retried by botocore (this default is 5). This "
            "does not take into account the number of exceptions retried by "
            "botocore."
        ),
    )
    group_s3_transfer.add_argument(
        "--s3-max-io-queue",
        dest="s3_max_io_queue_mb",
        default=100,
        type=int,
        help=(
            "The maximum amount of read parts that can be "
            "queued in memory to be written for a download. The size of each "
            "of these read parts is at most the size of ``io_chunksize``. (MB)"
        ),
    )
    group_s3_transfer.add_argument(
        "--s3-io-chunksize",
        dest="s3_io_chunksize_mb",
        default=1,
        type=int,
        help=(
            "The max size of each chunk in the io queue. "
            "Currently, this is size used when ``read`` is called on the "
            "downloaded stream as well. (MB)"
        ),
    )
    group_s3_transfer.add_argument(
        "--s3-progress",
        dest="s3_progress",
        action="store_true",
        help=(
            "Display upload/downloads progress in stdout. To be use with "
            "--mt-thread-size=1 not properly working otherwise"
        ),
    )


def parse_setup_s3(arguments):
    default_aws_s3_path = PureS3Path("/")
    params = {}
    if arguments.s3_endpoint_url:
        params["endpoint_url"] = arguments.s3_endpoint_url
    if arguments.s3_region_name:
        params["region_name"] = arguments.s3_region_name
    if arguments.s3_access_key_id:
        params["aws_access_key_id"] = arguments.s3_access_key_id
    if arguments.s3_secret_access_key:
        params["aws_secret_access_key"] = arguments.s3_secret_access_key

    params["config"] = Config(signature_version="s3v4")

    KB = 1024
    MB = KB**2
    GB = KB**3

    transfert_config_params = dict(
        use_threads=not arguments.s3_no_threads,
        multipart_threshold=arguments.s3_multipart_threshold_mb * MB,
        multipart_chunksize=arguments.s3_multipart_chunksize_mb * MB,
        max_bandwidth=arguments.s3_max_bandwidth_mb * MB
        if arguments.s3_max_bandwidth_mb
        else None,
        max_concurrency=arguments.s3_max_concurrency,
        num_download_attempts=arguments.s3_num_download_attempts,
        max_io_queue=arguments.s3_max_io_queue_mb * MB,
        io_chunksize=arguments.s3_io_chunksize_mb * MB,
    )

    class ProgressPercentage(object):
        """This is not working in multiprocessing
        to be use with --mt-thread-size=1
        """

        def __init__(self, filename):
            self._filename = filename
            self._size = filename.stat().st_size
            self._seen_so_far = 0
            self._lock = threading.Lock()

        def __call__(self, bytes_amount):
            # To simplify we'll assume this is hooked up
            # to a single filename.
            with self._lock:
                self._seen_so_far += bytes_amount
                percentage = (self._seen_so_far / self._size) * 100
                sys.stdout.write(
                    "\r%s  %.3f GB / %.3f GB  (%.2f%%)"
                    % (
                        self._filename,
                        self._seen_so_far / GB,
                        self._size / GB,
                        percentage,
                    )
                )
                sys.stdout.flush()

    if params:
        register_configuration_parameter(
            default_aws_s3_path,
            resource=boto3.resource("s3", **params),
            parameters={
                "StorageClass": "GLACIER",
                "transfert_config": TransferConfig(**transfert_config_params),
                "callback_class": ProgressPercentage if arguments.s3_progress else None,
            },
        )


def rule_params(parser):
    group = parser.add_argument_group("Search rule")
    group.add_argument(
        "-rf",
        "--rule-file",
        dest="rule_file",
        type=argparse.FileType("r"),
        help="Rule file, is a json/yaml file that contains a pattern "
        "rule list to use to retreive search files. "
        "If used other params are ignored.",
    )
    group.add_argument(
        "-g",
        "--glob-pattern",
        dest="glob_pattern",
        type=str,
        help="Pattern used to retreive files in first step '**/*' for any recursice files",
    )
    group.add_argument(
        "-i",
        "--include-regex-pattern",
        dest="include_regex_pattern",
        type=str,
        help="Pattern used to filter files files and kept those"
        "that match given regex pattern.",
        default=None,
    )
    group.add_argument(
        "-e",
        "--exlcude-regex-pattern",
        dest="exclude_regex_pattern",
        type=str,
        help="Pattern used to filter files files and remove those"
        "that match given regex pattern.",
        default=None,
    )
    group.add_argument(
        "-df",
        "--from-date",
        dest="from_date",
        # https://docs.python.org/3/library/datetime.html#datetime.datetime.fromisoformat
        type=datetime.fromisoformat,
        help="Filter files with changed date earlier to this given date.",
        default=None,
    )
    group.add_argument(
        "-dt",
        "--to-date",
        dest="to_date",
        # https://docs.python.org/3/library/datetime.html#datetime.datetime.fromisoformat
        type=datetime.fromisoformat,
        help="Filter files with changed date earlier to this given date.",
        default=None,
    )
    group.add_argument(
        "-ddf",
        "--delta-days-from",
        dest="delta_days_from",
        type=float,
        help="Apply delta time days on from date (use `now()` if from date is not define).",
        default=None,
    )
    group.add_argument(
        "-ddt",
        "--delta-days-to",
        dest="delta_days_to",
        type=float,
        help="Apply delta time days on to date (use `now()` if to date is not define).",
        default=None,
    )
    group.add_argument(
        "-wd",
        "--with-directories",
        dest="file_only",
        action="store_false",
        help="Also return matching directories (without this option only files are returns).",
        default=True,
    )


def default_rules() -> SearchPatternRules:
    glob_pattern = "**/*"
    return SearchPatternRules(
        rules=[
            SearchPattern(
                name="Once a year after a year",
                glob_pattern=glob_pattern,
                exclude_pattern=".*-[1,2][9,0,1,2][0-9]{2}-01-01T0[0-9]{5}.*",
                delta_from=timedelta(days=365),
                delta_to=None,
                file_only=True,
            ),
            SearchPattern(
                name="Once a month from 3 month to a year",
                glob_pattern=glob_pattern,
                exclude_pattern=".*-[1,2][9,0,1,2][0-9]{2}-[0,1][0-9]-01T0[0-9]{5}.*",
                delta_from=timedelta(days=90),
                delta_to=timedelta(days=365),
                file_only=True,
            ),
            SearchPattern(
                name="Twice a month from 1 month to 3 month (for 2 months)",
                glob_pattern=glob_pattern,
                exclude_pattern=".*-[1,2][9,0,1,2][0-9]{2}-[0,1][0-9]-(01|15)T0[0-9]{5}.*",
                delta_from=timedelta(days=31),
                delta_to=timedelta(days=90),
                file_only=True,
            ),
            SearchPattern(
                name="daily for a month",
                exclude_pattern=".*-[1,2][9,0,1,2][0-9]{2}-[0,1][0-9]-[0-3][0-9]T0[0-9]{5}.*",
                glob_pattern=glob_pattern,
                delta_from=timedelta(days=7),
                delta_to=timedelta(days=31),
                file_only=True,
            ),
        ]
    )


def parse_rule_args(arguments) -> SearchPatternRules:
    if arguments.rule_file:
        return rules_from_str(arguments.rule_file.read())
    else:
        if all(
            v is None
            for v in [
                arguments.glob_pattern,
                arguments.include_regex_pattern,
                arguments.exclude_regex_pattern,
                arguments.from_date,
                arguments.to_date,
                arguments.delta_days_from,
                arguments.delta_days_to,
            ]
        ):
            return default_rules()
        return SearchPatternRules(
            rules=[
                SearchPattern(
                    name="Rule from command line",
                    glob_pattern=arguments.glob_pattern,
                    include_pattern=arguments.include_regex_pattern,
                    exclude_pattern=arguments.exclude_regex_pattern,
                    from_date=arguments.from_date,
                    to_date=arguments.to_date,
                    delta_from=timedelta(days=arguments.delta_days_from)
                    if arguments.delta_days_from
                    else None,
                    delta_to=timedelta(days=arguments.delta_days_to)
                    if arguments.delta_days_to
                    else None,
                    file_only=arguments.file_only,
                )
            ]
        )


def rules_from_str(content) -> SearchPatternRules:
    """Content is SearchPatternRules to be deserialized by pydantic
    as far we are using yaml parser, you can define as json or yaml
    format"""
    return SearchPatternRules(**safe_load(content))
