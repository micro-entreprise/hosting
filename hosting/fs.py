import argparse
import json
import logging.config
import re
from datetime import datetime
from pathlib import Path
from typing import Callable, List, Union

from hosting.model import SearchPatternRules, SearchResult
from hosting.parser_util import (
    logging_params,
    parse_rule_args,
    parse_setup_s3,
    rule_params,
    s3_params,
)

logger = logging.getLogger(__name__)


def search():
    parser = argparse.ArgumentParser(
        description=(
            "Searching files with different criteria "
            "(expose get_latest_file and get_files method "
            "that you can use in sub shell command likes::\n"
            "  for f in `search-files`; do echo $f; done \n"
            "Details of the search algorithm:\n"
            "* use glob pattern to retreive files (and directories)\n"
            "* apply filter files (unless `--with-directories` we ensure is_file() "
            "return true which ensure current user is able to at least "
            "read the file. Also remove directories).\n"
            "* if include regex parttern provide use it\n"
            "* if exclude regex parttern provide use it\n"
            "* exclude files earlier than from-date\n"
            "* exclude files older than to-date\n"
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        action="store_true",
        help="Display extra informations likes file date, maching rules.",
        default=None,
    )
    parser.add_argument(
        "--last",
        dest="last",
        type=int,
        help="Display only last X changes.",
        default=None,
    )
    parser.add_argument(
        "directory",
        type=Path,
        help="Base directory where to perform the search",
    )
    logging_params(parser)
    s3_params(parser)
    rule_params(parser)
    arguments = parser.parse_args()
    logging.basicConfig(
        level=getattr(logging, arguments.logging_level.upper()),
        format=arguments.logging_format,
    )
    if arguments.logging_file:
        try:
            json_config = json.loads(arguments.logging_file.read())
            logging.config.dictConfig(json_config)
        except json.JSONDecodeError:
            logging.config.fileConfig(arguments.logging_file.name)

    parse_setup_s3(arguments)
    rules = parse_rule_args(arguments)
    res = get_files_from_rules(
        arguments.directory,
        rules,
    )

    dp = res.display(
        last=arguments.last,
        verbose=arguments.verbose,
    )
    print(dp)


def get_latest_file(
    directory: Union[Path, str, bytes],
    glob_pattern: str = "*",
    re_pattern: Union[str, re.Pattern] = None,
    exclude_re_pattern: Union[str, re.Pattern] = None,
    from_dt: datetime = None,
    to_dt: datetime = None,
) -> Path:
    """Return the latest modifiated file in given directory with given glob_pattern"""
    files = get_files(
        directory,
        glob_pattern=glob_pattern,
        re_pattern=re_pattern,
        exclude_re_pattern=exclude_re_pattern,
        from_dt=from_dt,
        to_dt=to_dt,
        file_only=True,
    )
    if len(files) == 0:
        return None
    return files[-1]


def get_files_from_rules(
    directory: Union[Path, str, bytes],
    patterns: SearchPatternRules,
    sorted_method: Callable = lambda found_file: found_file.file.stat().st_mtime,
    file_only: bool = True,
) -> SearchResult:
    results = SearchResult(root_directory=directory)
    for rule in patterns.rules:
        for file in get_files(
            directory,
            glob_pattern=rule.glob_pattern,
            re_pattern=rule.include_pattern,
            exclude_re_pattern=rule.exclude_pattern,
            from_dt=rule.from_dt,
            to_dt=rule.to_dt,
            sorted_method=None,
            file_only=rule.file_only,
        ):
            results.append(file, rule)

    if sorted_method:
        results.sort(key=sorted_method)
    return results


def get_files(
    directory: Union[Path, str, bytes],
    glob_pattern: str = "*",
    re_pattern: Union[str, re.Pattern] = None,
    exclude_re_pattern: Union[str, re.Pattern] = None,
    from_dt: datetime = None,
    to_dt: datetime = None,
    sorted_method: Callable = lambda p: p.stat().st_mtime,
    file_only: bool = True,
) -> List[Path]:
    """return only files for given glob_pattern oldest touched file is the first one"""
    if not glob_pattern:
        glob_pattern = "*"

    if isinstance(directory, str):
        directory = Path(directory)
    elif isinstance(directory, bytes):
        directory = Path(directory.decode())

    if not directory.is_dir():
        raise NotADirectoryError(f"Could you ensure directory exists: {directory}")

    if file_only:
        methods = [is_file_rule]
    else:
        methods = []

    if re_pattern is not None:
        if isinstance(re_pattern, str):
            re_pattern = re.compile(re_pattern)

        methods.append(match_regexp_rule(re_pattern, include=True))

    if exclude_re_pattern is not None:
        if isinstance(exclude_re_pattern, str):
            exclude_re_pattern = re.compile(exclude_re_pattern)

        methods.append(match_regexp_rule(exclude_re_pattern, include=False))

    if from_dt is not None:
        methods.append(date_rule(from_dt, "<"))

    if to_dt is not None:
        methods.append(date_rule(to_dt, ">"))

    files = [f for f in directory.glob(glob_pattern) if check_all_rules(methods, f)]

    if sorted_method:
        files = sorted(files, key=sorted_method)
    return files


def date_rule(dt: datetime, operator: str) -> callable:
    def filter_date(file: Path) -> bool:
        return eval(f"file.stat().st_mtime {operator} {dt.timestamp()}")

    return filter_date


def match_regexp_rule(re_pattern: re.Pattern, include: bool = True) -> callable:
    def match_regexp(file: Path) -> bool:
        """regex on file name (not absolute path)"""
        found = re_pattern.search(str(file)) is not None
        return found if include else not found

    return match_regexp


def is_file_rule(file: Path) -> bool:
    return file.is_file()


def check_all_rules(rules: List[callable], file: Path) -> bool:
    for rule in rules:
        if not rule(file):
            return False
    return True
