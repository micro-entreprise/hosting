import argparse
import socket

from prometheus_client import CollectorRegistry, Gauge, push_to_gateway
from prometheus_client.exposition import basic_auth_handler


def pushgateway_params(parser):
    prom_group = parser.add_argument_group("Prometheus push gateway params")
    prom_group.add_argument(
        "prometheus_gateway_uri",
        type=str,
        help=(
            "https uri to the prometheus gateway "
            "(ie: https://gatewat.prom.example.com)."
        ),
    )
    prom_group.add_argument(
        "prometheus_job_name",
        type=str,
        help=("Job name used to identify metric in " "the prometheus metric time seri."),
    )
    prom_group.add_argument(
        "--prometheus-metric",
        dest="prom_metric",
        type=str,
        default="scheduled_task_last_success_unixtime",
        help="Metric time seri name used to send gauge unix time.",
    )
    prom_group.add_argument(
        "--prometheus-instance",
        dest="prom_instance",
        type=str,
        default=socket.gethostname(),
        help=(
            "Instance name, default value computed with hostname "
            "where the script is running."
        ),
    )
    prom_group.add_argument(
        "--prometheus-user",
        dest="prom_user",
        type=str,
        help="User used to post metric over basic http auth",
    )
    prom_group.add_argument(
        "--prometheus-password",
        dest="prom_password",
        type=str,
        help="User password used to post metric over basic http auth",
    )


def notify():
    parser = argparse.ArgumentParser(
        description="Notify Prometheus pushgateway",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    pushgateway_params(parser)
    arguments = parser.parse_args()
    notify_pushgateway(
        gateway_uri=arguments.prometheus_gateway_uri,
        job_name=arguments.prometheus_job_name,
        metric=arguments.prom_metric,
        instance=arguments.prom_instance,
        user=arguments.prom_user,
        password=arguments.prom_password,
    )


def notify_pushgateway(
    gateway_uri: str = None,
    job_name: str = None,
    metric: str = None,
    instance: str = None,
    user: str = None,
    password: str = None,
):
    if any([user, password]):

        def my_auth_handler(url, method, timeout, headers, data):
            return basic_auth_handler(
                url, method, timeout, headers, data, user, password
            )

    else:
        my_auth_handler = None

    registry = CollectorRegistry()
    g = Gauge(metric, "Last time job successfully finished", registry=registry)
    g.set_to_current_time()

    grouping_key = {}
    if instance:
        grouping_key = {"instance": instance}

    push_to_gateway(
        gateway_uri,
        job=job_name,
        registry=registry,
        handler=my_auth_handler,
        grouping_key=grouping_key,
    )
