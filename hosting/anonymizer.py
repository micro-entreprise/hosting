import argparse
import json
import logging.config
import subprocess
from dataclasses import dataclass
from pathlib import Path

from hosting.fs import get_latest_file
from hosting.parser_util import logging_params, pg_params

DEFAULT_WRAP_CMD = "{command}"
logger = logging.getLogger(__name__)


def anonymize_params(parser):
    anonymize_group = parser.add_argument_group("Anonymize params")
    anonymize_group.add_argument(
        "input",
        type=Path,
        help="Input dump file (assume dump was made using -Fc -O options)"
        "if it's directory, it'll retreive the latest file with one of"
        "the following regex pattern `.*.(dump|sql.gz|sql)$`",
    )
    anonymize_group.add_argument(
        "sql",
        type=Path,
        help="SQL queries to run in order to anonymize the database",
    )
    anonymize_group.add_argument(
        "output",
        type=Path,
        help="Output dump file if it's a directory it'll use the same input file name",
    )
    anonymize_group.add_argument(
        "--force-overwrite",
        dest="force",
        action="store_true",
        help="Overwrite output even if output file exists (raise by default)",
    )


def docker_wrapper_params(parser):
    wrapp_group = parser.add_argument_group(
        "Wrapping pg commands. If you like to run pg_restore, "
        "psql, pg_dump commands inside a container. Mind to set pg "
        "in that context then."
    )
    wrapp_group.add_argument(
        "-w",
        "--wraping-template",
        type=str,
        help="format-string template to wrapp pg calls commands require "
        "this `{command}` placeholder "
        "(ie: docker exec dbname bash -c '{command}').",
        dest="wrap_pg_call_template",
        default=DEFAULT_WRAP_CMD,
    )


def anonymize(testing=False):
    parser = argparse.ArgumentParser(
        description="Anonymise SQL dump",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-n",
        "--dry",
        action="store_true",
        help="Do not perform call, display at Warning level",
    )
    anonymize_params(parser)
    logging_params(parser)
    pg_params(parser)
    docker_wrapper_params(parser)
    arguments = parser.parse_args()
    logging.basicConfig(
        level=getattr(logging, arguments.logging_level.upper()),
        format=arguments.logging_format,
    )
    if arguments.logging_file:
        try:
            json_config = json.loads(arguments.logging_file.read())
            logging.config.dictConfig(json_config)
        except json.JSONDecodeError:
            logging.config.fileConfig(arguments.logging_file.name)
    anon = Anonymize(
        input=arguments.input,
        sql=arguments.sql,
        output=arguments.output,
        pg_host=arguments.pg_host,
        pg_port=arguments.pg_port,
        pg_dbname=arguments.pg_dbname,
        pg_user=arguments.pg_user,
        pg_restore_extra=arguments.pg_restore_extra,
        pg_dump_extra=arguments.pg_dump_extra,
        psql_extra=arguments.psql_extra,
        dry_run=arguments.dry,
        wrap_cmd=arguments.wrap_pg_call_template,
        force=arguments.force,
    )
    anon.run()
    if testing:
        return anon


@dataclass
class Anonymize:
    input: Path = None
    sql: Path = None
    output: Path = None
    force: bool = None

    pg_host: str = None
    pg_port: str = None
    pg_dbname: str = None
    pg_user: str = None
    pg_restore_extra: str = None
    pg_dump_extra: str = None
    psql_extra: str = None
    dry_run: bool = None
    wrap_cmd: str = None

    @property
    def wrap_command(self):
        return self.wrap_cmd or DEFAULT_WRAP_CMD

    def run(self):
        if not isinstance(self.input, Path):
            raise FileNotFoundError(f"Input file not found ({self.input})")
        if self.input.is_dir():
            self.input = (
                get_latest_file(self.input, re_pattern=".*.(dump|sql.gz|sql)$") or Path()
            )
        if self.wrap_command == DEFAULT_WRAP_CMD and not self.input.exists():
            raise FileNotFoundError(f"Input file not found ({self.input})")

        if not isinstance(self.sql, Path) or (
            self.wrap_command == DEFAULT_WRAP_CMD and not self.sql.exists()
        ):
            raise FileNotFoundError(f"Anonymize SQL file not found ({self.sql})")

        if not isinstance(self.output, Path):
            raise ValueError(f"Output must be a pathlib.Path object ({self.output})")
        if self.output.is_dir():
            self.output = self.output.joinpath(self.input.name)
        if (
            not self.force
            and self.wrap_command == DEFAULT_WRAP_CMD
            and self.output.exists()
        ):
            raise FileExistsError(f"Output file already present ({self.output})")

        logger.info("Anonymizing '%s' dump using '%s'...", self.input, self.sql)
        try:
            self.restore()
            self.anonymize()
            self.dump()
        except (subprocess.TimeoutExpired, subprocess.CalledProcessError) as err:
            logger.error(
                "Couldn't anonymize `%s` because %s\n stdout: %s\n stderr: %s\n output: %s",
                self.input,
                str(err),
                (err.stdout or b"").decode(),
                (err.stderr or b"").decode(),
                (err.output or b"").decode(),
            )
            raise err
        logger.info("Anonymized dump ready: '%s'.", self.output)

    def restore(self):
        cmd = ["pg_restore", "-Fc", "-O"]
        cmd.extend(self.db_cnx)
        if self.pg_restore_extra:
            cmd.append(self.pg_restore_extra)
        cmd.append(str(self.input))
        logger.info("Restore '%s' dump into pgsql (%r)...", self.input, self.db_cnx)
        self.run_command(cmd)

    def anonymize(self):
        cmd = ["psql"]
        cmd.extend(self.db_cnx)
        if self.psql_extra:
            cmd.append(self.psql_extra)
        cmd.extend(["-f", str(self.sql)])
        logger.info("Anonymize db (%r) using '%s'...", self.db_cnx, self.sql)
        self.run_command(cmd)

    def dump(self):
        cmd = ["pg_dump", "-Fc", "-O"]
        cmd.extend(self.db_cnx)
        if self.pg_dump_extra:
            cmd.append(self.pg_dump_extra)
        cmd.extend(["-f", str(self.output)])
        logger.info("Dumping (%r) to '%s'...", self.db_cnx, self.output)
        self.run_command(cmd)

    @property
    def db_cnx(self):
        cmd = []
        if self.pg_host:
            cmd.extend(["-h", self.pg_host])
        if self.pg_port:
            cmd.extend(["-p", self.pg_port])
        if self.pg_user:
            cmd.extend(["-U", self.pg_user])
        if self.pg_dbname:
            cmd.extend(["-d", self.pg_dbname])
        return cmd

    def run_command(self, command, **kwargs):
        cmd = self.wrap_command.format(command=" ".join(command))
        if self.dry_run:
            logger.warning("Dry run mode, would call: %s", cmd)
            return None
        logger.debug("Running %s...", cmd)
        return subprocess.run(cmd, shell=True, check=True, capture_output=True, **kwargs)
