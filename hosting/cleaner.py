import argparse
import json
import logging.config
from dataclasses import dataclass
from pathlib import Path
from shutil import rmtree

from hosting.fs import get_files_from_rules
from hosting.model import SearchPatternRules
from hosting.parser_util import (
    logging_params,
    parse_rule_args,
    parse_setup_s3,
    rule_params,
    s3_params,
)

logger = logging.getLogger(__name__)


def main(testing: bool = False):
    parser = argparse.ArgumentParser(
        description="Purge directory archives",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-n",
        "--dry",
        action="store_true",
        help="Do not perform call, display at Warning level",
    )
    parser.add_argument(
        "directory",
        type=Path,
        help="Base directory where to perform the purge",
    )
    logging_params(parser)
    s3_params(parser)
    rule_params(parser)
    arguments = parser.parse_args()

    logging.basicConfig(
        level=getattr(logging, arguments.logging_level.upper()),
        format=arguments.logging_format,
    )
    if arguments.logging_file:
        try:
            json_config = json.loads(arguments.logging_file.read())
            logging.config.dictConfig(json_config)
        except json.JSONDecodeError:
            logging.config.fileConfig(arguments.logging_file.name)

    parse_setup_s3(arguments)
    patterns = parse_rule_args(arguments)

    purger = Purger(
        root_directory=arguments.directory,
        patterns=patterns,
        dry_run=arguments.dry,
    )
    purger.clean()
    if testing:
        return purger


@dataclass
class Purger:
    root_directory: Path = None
    """base directory where to apply the purge"""

    patterns: SearchPatternRules = None
    """List of patterns to apply on files
    """

    dry_run: bool = None
    """Do not perform remove/unlink actions and warn a message instead"""

    def clean(self):
        """keeps at least for the time being by default:
        - all files / days for 7 days
        - files dumped between [00-10[ on the morning / day for 30 days
        - days 1-15 for 90 days
        - 1st of month for 365 days
        - 1st of year for 10 years

        days to keep is applyed on mtime while regex patterns apply on
        file names.
        """
        saved_place = 0
        founds = get_files_from_rules(
            self.root_directory, self.patterns, sorted_method=lambda ff: str(ff.file)
        ).files
        for found in founds:
            saved_place += found.file.stat().st_size
            if self.dry_run:
                logger.warning(
                    "File or directory not removed (dry run mode): %s",
                    found.display(
                        verbose=True,
                        file_format_with=30,
                        root_dir=self.root_directory,
                    ),
                )
            else:
                logger.debug("Removing %s by %s rule...", found.file, found.rules_name)
                if found.file.is_dir():
                    # todo to make it works using S3Path we should
                    # consider unlink and rmdir recursively
                    rmtree(found.file)
                else:
                    found.file.unlink()
        logger.info(
            "Space %ssaved %s Gb by removing %s files.",
            "you would " if self.dry_run else "",
            saved_place / 1_000_000_000,
            len(founds),
        )
