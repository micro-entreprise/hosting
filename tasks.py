import os

from invoke import task

DBHOST = os.environ.get("DBHOST", "127.0.0.1")


@task
def lint(c):
    """Linting code"""
    c.run("pre-commit install --install-hooks")
    c.run("pre-commit run --all-files --show-diff-on-failure")


@task
def init_test(c):
    """Install test dependencies"""
    c.run("pip install -r requirements.txt")
    c.run("pip install -r requirements.tests.txt")
    c.run("pip install -e .")


@task(pre=[init_test])
def init_dev(c):
    """Init dev env with dev dependencies"""
    c.run("pip install -r requirements.dev.txt")
    c.run("pre-commit install --install-hooks")


@task
def test(c):
    """Launch pytest"""
    c.run("pytest")


@task(pre=[init_dev])
def build(c):
    """build tarball and wheel"""
    c.run("rm -rf dist/")
    c.run("python setup.py sdist bdist_wheel")
    c.run("twine check dist/*")
    c.run("pip install dist/*.whl")
    c.run("hosting-anonymizer --help")
    c.run("hosting-purge --help")
    c.run("hosting-search --help")
    c.run("hosting-rules-helper --help")
    c.run("pip install -e .")


@task(post=[build])
def release(c, part):
    """Bump version part (major.minor.patch) and build"""
    c.run("bump2version", part)


@task
def push_on_pypi(c):
    """push to any wheel house (TODO)"""
    c.run("twine upload dist/*")


def start_pgsql(c):
    """starting docker compose and wait db to be ready"""
    c.run("docker compose up -d")
    c.run(
        "docker compose exec -T db-pg-anonymizer sh -c "
        '\'while ! psql -U anouser -d dbano -c "select 1"; '
        "do sleep 1; done;'"
    )
    c.run(
        "docker compose exec -T db sh -c "
        '\'while ! psql -U anouser -d dbano -c "select 1"; '
        "do sleep 1; done;'"
    )


def cleanup_before_integration(c):
    """test using postgresql over port connexion, require postgresql-client install on the host"""
    # ensure docker env is empty
    c.run("docker compose down -v --remove-orphans")
    c.run("rm -rf integration/tests/")
    # create empty directory to ensure access write to the current user
    c.run("mkdir -p integration/tests/")
    # start pg_server in docker container
    start_pgsql(c)


def assert_integraion_results(c):
    # create new database and restore fresh dump in it
    c.run(
        "docker compose exec -T db sh -c '"
        "createdb -U anouser dbano_ano "
        "&& pg_restore  -U anouser -d dbano_ano -Fc -O /integration/tests/anonymized.dump'"
    )
    res = c.run(
        'docker compose exec -T db sh -c "'
        'psql -A -d dbano_ano -U anouser -t -c \\"'
        'SELECT count(*) FROM person;\\""'
    )
    assert res.ok
    assert int(res.stdout) == 3
    res = c.run(
        'docker compose exec -T db sh -c "'
        'psql -A -d dbano_ano -U anouser -t -c \\"'
        "SELECT count(*) FROM person WHERE first_name='Pierre' and last_name='Verkest';\\\"\""
    )
    assert res.ok
    assert int(res.stdout) == 0
    res = c.run(
        'docker compose exec -T db-pg-anonymizer sh -c "'
        'psql -A -d dbano -U anouser -t -c \\"'
        '\\\dn \\""'  # noqa: W605
    )
    assert res.ok
    assert (
        len([line for line in res.stdout.split("\n") if line]) == 2
    ), "Expected 2 schema: public and private"
    res = c.run(
        'docker compose exec -T db sh -c "'
        'psql -A -d dbano_ano -U anouser -t -c \\"'
        '\\\dn \\""'  # noqa: W605
    )
    assert res.ok
    assert (
        len([line for line in res.stdout.split("\n") if line]) == 1
    ), "Expected 1 schema: public"


@task()
def integration_port(c):
    cleanup_before_integration(c)
    # run script (which restore -> anonymize -> dump)
    c.run(
        "export PGPASSWORD=anopassword;"
        "hosting-anonymizer "
        "-l DEBUG "
        "--extra-pg-dump '--exclude-schema private --no-security-labels -Fc -O' "
        f"-H {DBHOST} -p 4532 -U anouser -d dbano "
        "./integration/ ./integration/integration.sql.anon ./integration/tests/anonymized.dump"
    )
    assert_integraion_results(c)


@task()
def integration_wrapped(c):
    """Run integration test (require docker)
    `integraion/integration.dump` was setup like this

     create table person (first_name varchar, last_name varchar);
     insert into person values ('Pierre', 'Verkest'), ('John', 'Doe'), ('Rémy', 'Le Nom Composé');

     create schema private;
     create table private.person (first_name varchar, last_name varchar);
     insert into private.person values ('Gaston', 'Lagaffe');
    """
    cleanup_before_integration(c)
    # run script (which restore -> anonymize -> dump)
    c.run(
        "hosting-anonymizer -w 'docker compose exec -T db-pg-anonymizer sh -c \"{command}\"' "
        "-l DEBUG "
        "--extra-pg-dump '--exclude-schema private --no-security-labels -Fc -O' "
        "-U anouser -d dbano "
        "/integration/integration.dump /integration/integration.sql.anon /integration/tests/anonymized.dump"
    )
    assert_integraion_results(c)


@task(pre=[integration_wrapped, integration_port])
def integrations(c):
    pass


@task()
def clean(c):
    """clean"""
    c.run("docker compose down -v --remove-orphan")
    c.run("rm -fr dist/")
