# hosting

Package that contains tools that I use to manage my
cloud VMs.

##  install master build

```pip install hosting --extra-index-url https://gitlab.com/api/v4/projects/25806119/packages/pypi/simple```

> **Note**: docker compose is to be installed manually en dev env

## Features

### S3 compatible

Based on python [s3path library](https://github.com/liormizr/s3path) it abstract
the way to manage s3 files based on pathlib interfaces.

S3 exchanges are based on boto3.

To use s3 directory instead FileSystem directory start your directory with `s3://`.

> **Warning**: use this care has s3 operations rely on your networks
> ie on glob operations https://github.com/liormizr/s3path/issues/85.


> **None**: purging directory is not yet supported.


### Command line features

* **hosting-prom-notify**: script to send last success scheduled task unix time to
  prometheus gateway in order to configure alerting if last success date is greater than
  expected.

* **hosting-anonymizer**: wrap calls to restore > execute SQL Script > dump a database.

> **WARNING**: by default psql return 0 code even your script failled
> it's up to you to properly configure `\set ON_ERROR_STOP true`
> and `BEGIN/COMMIT` transaction in your provided sql script.

* **hosting-purge**: remove old files (or directory) in a directory for given:
  * glob pattern search (default recursive `**/*`)
  * set of rules based on:
    * date range (relative days or fixed date)
    * regex pattern (include or exclude)

* **hosting-search**: search files (or directories) for given set of rules.
  the stdout result can be used by other software:

```bash
for f in `hosting-search --last 2 -g "**/*.py" ./`; do echo "doing something with 2 last changes $f"; done
doing something with 2 last changes hosting/fs.py
doing something with 2 last changes tests/test_fs.py
```

* **hosting-rules-helper**: tools to help writing search rules

### Rule format

As far json is a subset of yaml you can define your rule either in
yaml or json format. Those are default values:

```yaml
rules:
- name: Once a year after a year
  glob_pattern: '**/*'
  delta_from: 31536000.0
  delta_to: null
  exclude_pattern: .*-[1,2][9,0,1,2][0-9]{2}-01-01T0[0-9]{5}.*
  from_date: null
  include_pattern: null
  to_date: null
- name: Once a month from 3 month to a year
  glob_pattern: '**/*'
  delta_from: 7776000.0
  delta_to: 31536000.0
  exclude_pattern: .*-[1,2][9,0,1,2][0-9]{2}-[0,1][0-9]-01T0[0-9]{5}.*
  from_date: null
  include_pattern: null
  to_date: null
- name: Twice a month from 1 month to 3 month (for 2 months)
  glob_pattern: '**/*'
  delta_from: 2678400.0
  delta_to: 7776000.0
  exclude_pattern: .*-[1,2][9,0,1,2][0-9]{2}-[0,1][0-9]-(01|15)T0[0-9]{5}.*
  from_date: null
  include_pattern: null
  to_date: null
- name: daily for a month
  glob_pattern: '**/*'
  delta_from: 604800.0
  delta_to: 2678400.0
  exclude_pattern: .*-[1,2][9,0,1,2][0-9]{2}-[0,1][0-9]-[0-3][0-9]T0[0-9]{5}.*
  from_date: null
  include_pattern: null
  to_date: null
```

### Python main API

* `hosting.fs.get_files_from_rules`: call get_files given a list of rules
   define in `hosting.model.SearchPatternRules` object
* `hosting.fs.get_files`: retrieve files and sorted by whiched method
   for a given glob search and different filters based on date and regex.

## Roadmap

- [ ] manage scaleway volume snapshot purge
